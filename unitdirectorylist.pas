unit unitdirectorylist;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, SynEdit, LResources, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, Buttons, StdCtrls, EditBtn;

type

  { TDirectories }

  TDirectories = class(TForm)
    Label1: TLabel;
    LogsDirStatusLabel: TLabel;
    Button1: TButton;
    LogsDirectoryButton: TBitBtn;
    LogsDirectoryEdit: TEdit;
    ResetToLogsDirectoryButton: TBitBtn;
    TZdatabasepathDisplay: TLabeledEdit;
    FirmwareFilesPathDisplay: TLabeledEdit;
    DataDirectoryDisplay: TLabeledEdit;
    ConfigfilePathDisplay: TLabeledEdit;
    procedure FormCreate(Sender: TObject);
    procedure LogsDirectoryButtonClick(Sender: TObject);
    procedure LogsDirectoryDisplayChange(Sender: TObject);
    procedure ResetToLogsDirectoryButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    procedure ValidateLogsDirectory();
  public
    { public declarations }
  end;

var
  Directories: TDirectories;

implementation

uses Unit1,appsettings;

{ TDirectories }

procedure TDirectories.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TDirectories.FormCreate(Sender: TObject);
begin
  {Clear out logs directory check status message}
  LogsDirStatusLabel.Caption:='';
end;

procedure TDirectories.LogsDirectoryButtonClick(Sender: TObject);
begin
  if (SelectDirectory('Select the logs directory',LogsDirectoryEdit.Text, appsettings.LogsDirectory)) then begin
    {Assign setting}
    LogsDirectoryEdit.Text:=appsettings.LogsDirectory;
    {Check validity}
    ValidateLogsDirectory();
  end;
end;

procedure TDirectories.ValidateLogsDirectory();
begin
  {Check validity}
  if DirectoryExists(appsettings.LogsDirectory) then begin
    {Remove warning}
    LogsDirStatusLabel.Caption:='';
    {Save setting}
    vConfigurations.WriteString('Directories','LogsDirectory',appsettings.LogsDirectory);
  end
  else
    {Show warning}
    LogsDirStatusLabel.Caption:='Directory does not exist!';
end;

procedure TDirectories.LogsDirectoryDisplayChange(Sender: TObject);
begin
  {Allow changes to setting}
  appsettings.LogsDirectory:=LogsDirectoryEdit.Text;

  {Check validity}
  ValidateLogsDirectory();
end;

{ Reset logs directory }
procedure TDirectories.ResetToLogsDirectoryButtonClick(Sender: TObject);
begin
  {Reset}
  appsettings.LogsDirectoryReset();
  {Update display}
  LogsDirectoryEdit.Text:= RemoveMultiSlash(appsettings.LogsDirectory);
  {Save setting}
  vConfigurations.WriteString('Directories','LogsDirectory',appsettings.LogsDirectory);
end;

procedure TDirectories.FormShow(Sender: TObject);
begin

  LogsDirectoryEdit.Text:=RemoveMultiSlash(appsettings.LogsDirectory);
  TZdatabasepathDisplay.Caption:=appsettings.TZDirectory;
  FirmwareFilesPathDisplay.Caption:=appsettings.FirmwareDirectory;
  DataDirectoryDisplay.Caption:=appsettings.DataDirectory;
  ConfigfilePathDisplay.Caption:=appsettings.ConfigFilePath;

end;

initialization
  {$I unitdirectorylist.lrs}

end.

