unit worldmap;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls
  //, Types
  ;

type

  { TFormWorldmap }

  TFormWorldmap = class(TForm)
    ActualElevationEdit: TLabeledEdit;
    ActualLatitudeEdit: TLabeledEdit;
    ActualLongitudeEdit: TLabeledEdit;
    ApplyButton: TButton;
    CloseButton: TButton;
    CursorLatitude: TLabeledEdit;
    CursorLongitude: TLabeledEdit;
    DesiredElevationEdit: TLabeledEdit;
    DesiredLabel: TLabel;
    AppliedLabel: TLabel;
    DesiredLatitudeEdit: TLabeledEdit;
    DesiredLongitudeEdit: TLabeledEdit;
    MapImage: TImage;
    CreditLabel: TLabel;
    CursorLabel: TLabel;
    LatitudeLabel: TLabel;
    LongitudeLabel: TLabel;
    ElevationLabel: TLabel;
    UsageInstructions: TLabel;
    procedure ApplyButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure DesiredElevationEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MapImageClick(Sender: TObject);
    procedure MapImageMouseEnter(Sender: TObject);
    procedure MapImageMouseLeave(Sender: TObject);
    procedure MapImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer
      );
    procedure DesiredLatitudeEditChange(Sender: TObject);
    procedure DesiredLongitudeEditChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure WorldmapShow(CalledSection:String; CalledPostion:String);
  end;

//Declare local procedures
procedure UpdateLocationLines();
procedure CheckApplyButton();

var
  FormWorldmap: TFormWorldmap;
  TempLat, TempLon: Extended;

implementation

uses
  //convertlogfileunit
  appsettings //for file locations
  , dlheader //contains the DLHeader code
  , plotter
  , Unit1
  ;

const
  mapfile = 'world.topo.bathy.200412.3x800x400.jpg';
var
  WorkingSection:String;
  WorkingPosition:String;

{ TFormWorldmap }

procedure TFormWorldmap.FormShow(Sender: TObject);
var
  LatitudeString, LongitudeString, ElevationString: String;
begin

  //Get lat/lon displayed on this window
  TempLat:=MyLatitude;
  TempLon:=MyLongitude;

  LatitudeString:=Format('%0.6f',[TempLat]);
  DesiredLatitudeEdit.Text:=LatitudeString;
  ActualLatitudeEdit.Text:=LatitudeString;

  LongitudeString:=Format('%0.6f',[TempLon]);
  DesiredLongitudeEdit.Text:=LongitudeString;
  ActualLongitudeEdit.Text:=LongitudeString;

  ElevationString:=Format('%0.0f',[MyElevation]);
  DesiredElevationEdit.Text:=ElevationString;
  ActualElevationEdit.Text:=ElevationString;

  UpdateLocationLines();

  //ApplyButton.Enabled:=False;
  CheckApplyButton();

end;

procedure TFormWorldmap.MapImageClick(Sender: TObject);
begin
  DesiredLatitudeEdit.Text:=CursorLatitude.Caption;
  DesiredLongitudeEdit.Text:=CursorLongitude.Caption;

  TempLat:=StrToFloatDef(DesiredLatitudeEdit.text,0);
  TempLon:=StrToFloatDef(DesiredLongitudeEdit.text,0);

  UpdateLocationLines();
  CheckApplyButton();

  //ApplyButton.Enabled:=True;
end;

procedure TFormWorldmap.MapImageMouseEnter(Sender: TObject);
begin
  //Enable cursor display
  CursorLatitude.Visible:=True;
  CursorLongitude.Visible:=True;
  CursorLabel.Visible:=True;
end;

procedure TFormWorldmap.MapImageMouseLeave(Sender: TObject);
begin
  //Disable cursor display
  CursorLatitude.Visible:=False;
  CursorLongitude.Visible:=False;
  CursorLabel.Visible:=False;
end;

procedure TFormWorldmap.ApplyButtonClick(Sender: TObject);
var
  LatitudeString, LongitudeString, ElevationString, LocationString: String;
begin
  //Latitude
  TempLat:=StrToFloat(DesiredLatitudeEdit.Text);
  MyLatitude:=TempLat;
  ActualLatitudeEdit.Text:=FloatToStr(TempLat);
  LatitudeString:=FloatToStr(TempLat);

  //Longitude
  TempLon:=StrToFloat(DesiredLongitudeEdit.Text);
  MyLongitude:=TempLon;
  ActualLongitudeEdit.Text:=FloatToStr(TempLon);
  LongitudeString:=FloatToStr(TempLon);

  //Elevation. Ignore decimal point as per IDA/ChrisKyba header spec.
  MyElevation:= round(StrToFloatDef(DesiredElevationEdit.Text,0));
  ActualElevationEdit.Text:=Format('%0.0f',[MyElevation]);
  ElevationString:=DesiredElevationEdit.Text;

  LocationString:=
     LatitudeString + ', ' +
     LongitudeString + ', ' +
     ElevationString;

  //Set position values on called screen.
  //Also, write position to the associstaed section in the ini file.
  { TODO : changed to called function }
  if WorkingSection='DLHeader' then begin
    DLHeaderForm.PositionEntry.Text:=LocationString;
    vConfigurations.WriteString(SerialINISection,'Position',LocationString);
  end;
  if WorkingSection='Plotter' then begin
    PlotterForm.PositionEntry.Text:=LocationString;
    vConfigurations.WriteString(PlotterINISection,'Position',LocationString);
  end;


  Application.ProcessMessages;
  CheckApplyButton();
  UpdateLocationLines();
end;

procedure TFormWorldmap.CancelButtonClick(Sender: TObject);
begin
    Close;
end;

procedure TFormWorldmap.CloseButtonClick(Sender: TObject);
begin
    Close;
end;

procedure TFormWorldmap.DesiredElevationEditChange(Sender: TObject);
begin
  CheckApplyButton();
end;

procedure TFormWorldmap.DesiredLatitudeEditChange(Sender: TObject);
begin
  CheckApplyButton();
end;

procedure TFormWorldmap.DesiredLongitudeEditChange(Sender: TObject);
begin
  CheckApplyButton();
end;


procedure TFormWorldmap.MapImageMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  with FormWorldmap.MapImage do begin
    CursorLatitude.Caption:=format('%0.6f',[(Y - Height/2) * -90/(Height/2)],Unit1.FPointSeparator);
    CursorLongitude.Caption:=format('%0.6f',[(X - Width/2) * 180/(Width/2)],Unit1.FPointSeparator);
  end;
end;

procedure CheckApplyButton();
var
  Different: Boolean;
begin
  with FormWorldmap do begin
    if DesiredLatitudeEdit.Text <> ActualLatitudeEdit.Text then begin
      Different:=True;
      ActualLatitudeEdit.Font.Color:=clRed;
    end
    else
      ActualLatitudeEdit.Font.Color:=clDefault;

    if DesiredLongitudeEdit.Text <> ActualLongitudeEdit.Text then begin
      Different:=True;
      ActualLongitudeEdit.Font.Color:=clRed;
    end
    else
      ActualLongitudeEdit.Font.Color:=clDefault;

    if DesiredElevationEdit.Text <> ActualElevationEdit.Text then begin
      Different:=True;
      ActualElevationEdit.Font.Color:=clRed;
    end
    else
      ActualElevationEdit.Font.Color:=clDefault;

    ApplyButton.Enabled:=Different;

  end;
end;

procedure UpdateLocationLines();
var
  LatitudePostion, LongitudePostion : Integer;
begin

  with FormWorldmap.MapImage do begin
    Picture.LoadFromFile(appsettings.DataDirectory+mapfile);
    LatitudePostion:=round(Height/2 - (TempLat *  Height/2)/90 );
    LongitudePostion:=round(Width/2 + (TempLon *  Width/2)/180);

    Picture.Bitmap.Canvas.Pen.Color:=clWhite;
    Picture.Bitmap.Canvas.Line(0,LatitudePostion, Width,LatitudePostion);
    Picture.Bitmap.Canvas.Line(LongitudePostion,0, LongitudePostion,Height);
  end;

end;

procedure TFormWorldmap.WorldmapShow(CalledSection:String; CalledPostion:String);
begin
  WorkingSection:=CalledSection;
  WorkingPosition:=CalledPostion;
  ShowModal;
end;

initialization
  {$I worldmap.lrs}

end.

