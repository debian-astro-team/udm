unit comterm;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls
  , LCLType
  ,header_utils;

type

  { TComTermForm }

  TComTermForm = class(TForm)
    ClearButton: TButton;
    InputEdit: TEdit;
    Label1: TLabel;
    InputMemo: TMemo;
    OutputMemo: TMemo;
    procedure ClearButtonClick(Sender: TObject);
    procedure InputEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  ComTermForm: TComTermForm;

implementation

{ TComTermForm }
procedure TComTermForm.InputEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=VK_RETURN) then begin //remember to add LCLType in uses
  //writeln('Enter key');
  InputMemo.Append(InputEdit.Text);
  OutputMemo.Append(SendGet(InputEdit.Text));
  Key:=0;
  end;
end;

procedure TComTermForm.ClearButtonClick(Sender: TObject);
begin
  OutputMemo.Clear;
  InputMemo.Clear;
  InputEdit.Clear;
  InputEdit.SetFocus;
end;

initialization
  {$I comterm.lrs}

end.

