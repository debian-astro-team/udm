unit About;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls,LCLIntf;

type

  { TForm4 }

  TForm4 = class(TForm)
    Button1: TButton;
    Image1: TImage;
    WrittenByString: TLabel;
    FpointLabel: TLabel;
    FpointString: TLabel;
    FcommaLabel: TLabel;
    FCommaString: TLabel;
    WrittenByLabel: TLabel;
    VersionLabel: TLabel;
    FileVersionText: TLabel;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WrittenByStringClick(Sender: TObject);
    procedure WrittenByStringMouseEnter(Sender: TObject);
    procedure WrittenByStringMouseLeave(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form4: TForm4; 

implementation
uses
  vinfo,
  unit1;

{ TForm4 }

procedure TForm4.FormShow(Sender: TObject);
Var
  Info:     TVersionInfo;
Begin

  FpointString.Caption:=FPointSeparator.DecimalSeparator;
  FCommaString.Caption:=FCommaSeparator.DecimalSeparator;

  // grab the build number .... save it to BuildNum
  Info := TVersionInfo.Create;
  Info.Load(HINSTANCE);
  FileVersionText.Caption:=IntToStr(Info.FixedInfo.FileVersion[0])+'.'+
      IntToStr(Info.FixedInfo.FileVersion[1])+'.'+
      IntToStr(Info.FixedInfo.FileVersion[2])+'.'+
      IntToStr(Info.FixedInfo.FileVersion[3]);
  Info.Free;
       //ShowMessage(
       //          'Serial Library verion: ' +ser.GetVersion + sLineBreak +
       //          '' );

end;

procedure TForm4.WrittenByStringClick(Sender: TObject);
begin
  OpenURL(WrittenByString.Caption);
end;

procedure TForm4.WrittenByStringMouseEnter(Sender: TObject);
begin
  WrittenByString.Cursor := crHandPoint;
  WrittenByString.Font.Color := clBlue;
  WrittenByString.Font.Style := [fsUnderline];
  if Pos('http://www.', WrittenByString.Caption) = 0 then
    WrittenByString.Caption := 'http://www.' + WrittenByString.Caption;
end;

procedure TForm4.WrittenByStringMouseLeave(Sender: TObject);
begin
  WrittenByString.Font.Style := [];
  if Pos('http://www.', WrittenByString.Caption) > 0 then
    WrittenByString.Caption := Copy(WrittenByString.Caption, Pos('http://www.', WrittenByString.Caption) + Length('http://www.'), Length(WrittenByString.Caption));

end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  Form4.Close;
end;


initialization
  {$I about.lrs}

end.

