#!/usr/bin/python3

import os

file_in = 'udm.app/Contents/Info.plist'

''' BEFORE
<dict>
  <key>CFBundleDevelopmentRegion</key>
  <string>English</string>
'''

''' AFTER
<dict>
  <key>CFBundleIconFile</key>
  <string>udm.icns</string>
  <key>CFBundleDevelopmentRegion</key>
  <string>English</string>
'''

icon_str = "<dict>\n <key>CFBundleIconFile</key>\n <string>udm.icns</string>\n"

with open(file_in) as f:
  lines = f.readlines()
  all_lines = ''.join(lines)

  # Locate '<dict>'
  i_dict = all_lines.find("<dict>\n")

  # Locate ' <key>CFBundleDevelopmentRegion</key>'
  cfb_dict = all_lines.find(" <key>CFBundleDevelopmentRegion</key>")

  # Insert icon key and string lines between them
  print(all_lines[:(i_dict)] + icon_str + all_lines[(cfb_dict):])
f.close()

