unit splash;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls;

type

  { TfrmSplash }

  TfrmSplash = class(TForm)
    Image2: TImage;
    Label1: TStaticText;
    StaticText1: TStaticText;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmSplash: TfrmSplash;
  x:Integer;

implementation

{ TfrmSplash }

procedure TfrmSplash.Timer1Timer(Sender: TObject);
begin
  if x < 12 then
    x:= x+1
  else
    frmSplash.Close;

end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  x:= 0
end;

initialization
  {$I splash.lrs}

end.

