NAME=udm

# To install, type:
#  su -c "make install"

#Get version number from source file
#VERSION=$(shell head -3 $(NAME) | grep version | cut -d\" -f2)

#DESTDIR can be defined when calling make ie. make install DESTDIR=$RPM_BUILD_ROOT
DESTDIR =

prefix  = /usr
bindir  = $(prefix)/local/bin
datadir = $(prefix)/share

all:
	@echo "lp2m	Copy Linux published files to the Mac"
	@echo "distmac	makes mac distribution"
	@echo "deb	makes .deb for Debian (Linux), must be su."
	@echo "dist	makes tar.gz for Linux (old, does not include database files)"
	@echo "l64get	get pub files while in Linux64"
	@echo "l64put	put Linux64 deb file onto CD"


# Put Linux64 deb file onto CD
l64put:
	mv /home/anthony/udm/*.deb /media/sf_cd


# Get pub files while in Linux64"
l64get:
	cp /media/sf_pub/* /home/anthony/udm
	cp -r /media/sf_firmware/* /home/anthony/udm/firmware
	cp -r /media/sf_tzdatabase/* /home/anthony/udm/tzdatabase


#Copy Linux published files to the Mac
lp2m:
	scp -B /home/anthony/projects/sqm-le/lazarus/pub/* anthony@mac.local:/Users/anthony/projects/udm

#Not normally used directly, called by make deb:
install:
	install -D -m0755 udm $(DESTDIR)$(bindir)/$(NAME)
	install -d -m0755 $(DESTDIR)$(datadir)/$(NAME)
	install -d -m0755 $(DESTDIR)$(datadir)/$(NAME)/tzdatabase/
	install -m0644 tzdatabase/* $(DESTDIR)$(datadir)/$(NAME)/tzdatabase/
	install -d -m0755 $(DESTDIR)$(datadir)/$(NAME)/firmware/
	install -p -m0644 firmware/*.hex $(DESTDIR)$(datadir)/$(NAME)/firmware/
	install -m0644 fchanges.txt $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 world*.jpg $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 kmllegend*.png $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 changelog.txt $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 commandlineoptions.txt $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 sqm-skeleton.html $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 *.wav $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 *.ucld $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 *.goto $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 *.tcl $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 47_SKYGLOW_DEFINITIONS.PDF $(DESTDIR)$(datadir)/$(NAME)/
	install -m0644 udm.png /usr/share/pixmaps
	install -m0644 unihedron-device-manager.desktop /usr/share/applications

#Only used to remove installation of code.
uninstall:
	rm -f  $(DESTDIR)$(bindir)/udm
	rm -rf $(DESTDIR)$(datadir)/udm*

#Normally used to create a debian package
deb:
	checkinstall -D --pkgname=udm --maintainer='Anthony Tekatch \<anthony@unihedron.com\>' --provides='' --nodoc make install 

#Used to create a tar.gz file for Linux
dist:
	if test -d "$(NAME)-$(VERSION)"; then rm -rf $(NAME)-$(VERSION); fi
	if test -f "$(NAME)-$(VERSION).tar.gz"; then rm -f $(NAME)-$(VERSION).tar.gz; fi
	mkdir $(NAME)-$(VERSION)
	cp Makefile $(NAME)-$(VERSION)
	cp -R doc $(NAME)-$(VERSION)
	cp $(NAME) $(NAME)-$(VERSION)
	tar cvzf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION)
	rm -rf $(NAME)-$(VERSION)

#Used to create a dmg for Mac
distmac:
	@echo "Assumes that udm.app directory exists"
	@echo " which was created by Create Application Bundle in Lazarus" 
	cp -R firmware "udm.app/Contents/Resources/"
	cp -R -f tzdatabase "udm.app/Contents/Resources/"
	cp -f fchanges.txt "udm.app/Contents/Resources/"
	cp -f world*.jpg "udm.app/Contents/Resources/"
	cp -f kmllegend*.png "udm.app/Contents/Resources/"
	cp -f -p *.icns "udm.app/Contents/Resources/"
	cp -f 47_SKYGLOW_DEFINITIONS.PDF "udm.app/Contents/Resources/"
	cp -f *.wav "udm.app/Contents/Resources/"
	cp -f *.ucld "udm.app/Contents/Resources/"
	cp -f *.goto "udm.app/Contents/Resources/"
	cp -f *.tcl "udm.app/Contents/Resources/"
	cp -f changelog.txt "udm.app/Contents/Resources/"
	cp -f sqm-skeleton.html "udm.app/Contents/Resources/"
	rm "udm.app/Contents/MacOS/udm"
	cp udm "udm.app/Contents/MacOS/"
	./icon_str.py > Info.plist.temp
	cp Info.plist.temp udm.app/Contents/Info.plist
	rm Info.plist.temp
	rm -rf /Applications/udm.app
	cp -R udm.app /Applications/
	@echo "Now use Packages.app to create a .pkg file"
