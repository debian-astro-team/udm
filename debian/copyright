Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: udm
Source: http://unihedron.com/projects/darksky/cd/udm/

Files: *
Copyright: Unihedron
License: LGPL-2

Files: tzdatabase/*
Copyright: nobody
License: public-domain
 all files in the tz code and data are in the public domain.

Files: tzdatabase/date.c tzdatabase/newstrftime.3 tzdatabase/strftime.c
Copyright: 1989, 1991 The Regents of the University of California.
License: BSD-3

Files: aboutplaysound.pas
Copyright: 2014 Gordon Bamber minesadorada@charcodelvalle.com
License: LGPL-2+

Files: uplaysound.pas
Copyright: 2014 minesadorada@charcodelvalle.com
License: LGPL-2+

Files: jedi.inc
Copyright: Project JEDI
License: MPL-1.1 or LGPL-2+

Files: uglyfont.pas
Copyright: 2005 Soji Yamakawa
License: soji

Files: dynmatrix.pas
Copyright: 2008-2012 Paulo Costa
License: LGPL-2

Files: playwavepackage.lpk
Copyright: 2014 minesadorada@charcodelvalle.com
License: LGPL-2+

Files: dnssend.pas
	ssos2ws1.inc
	asn1util.pas
	synaip.pas
	synamisc.pas
	tlntsend.pas
	ssdotnet.inc
	synacode.pas
	ftpsend.pas
	synadbg.pas
	httpsend.pas
	sslinux.pas
	mimeinln.pas
	pingsend.pas
	synsock.pas
	synaicnv.pas
	ssl_streamsec.pas
	snmpsend.pas
	ldapsend.pas
	ssfpc.pas
	synafpc.pas
	ssl_openssl.pas
	nntpsend.pas
	synachar.pas
	imapsend.pas
	pop3send.pas
	sswin32.inc
	ssdotnet.pas
	synautil.pas
	ssl_openssl_lib.pas
	ftptsend.pas
	sslinux.inc
	clamsend.pas
	sswin32.pas
	blcksock.pas
	smtpsend.pas
	synacrypt.pas
	mimemess.pas
	ssl_cryptlib.pas
	ssl_sbb.pas
	ssfpc.inc
	slogsend.pas
	ssposix.inc
	sntpsend.pas
	synaser.pas
	ssl_libssh2.pas
Copyright: 1999-2014, Lukas Gebauer
Comment: Portions created by Petr Fejfar (2011-2012).
         Portions created by Hernan Sanchez (2000).
         Portions created by Hans-Georg Joepgen (2002).
         Portions created by Alexey Suhinin (2012-2013).
License: BSD-3-Gebauer
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of Lukas Gebauer nor the names of its contributors may
 be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the author nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: soji
 I don't abandon the copyright, but you can use this code and the header
 (uglyfont.cpp and uglyfont.h) for your product regardless of the purpose,
 i.e., free or commercial, open source or proprietary.
 .
 However, I do not take any responsibility for the consequence of using
 this code and header.  Please use on your own risks.

License: MPL-1.1
 On Debian systems, the complete text of the MPL-1.1
 can be found in `/usr/share/common-licenses/MPL-1.1'.

License: LGPL-2
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

