unit textfileviewer;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, SynMemo, LResources, Forms, Controls, Graphics,
  Dialogs
  ,appsettings
  ;
type

  { TTextFileViewerForm }

  TTextFileViewerForm = class(TForm)
    SynMemo1: TSynMemo;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  TextFileViewerForm: TTextFileViewerForm;
procedure fillview(Title: String; Filename: String);

implementation
procedure fillview(Title: String; Filename: String);
var
 File1: TextFile;
 Str: String;
begin
  {Display Filename }
  if FileExists(appsettings.DataDirectory+Filename) then
    begin
      AssignFile(File1,appsettings.DataDirectory+Filename);
      {$I-}//Temporarily turn off IO checking
      try
        Reset(File1);
        TextFileViewerForm.SynMemo1.Clear;
        repeat
          Readln(File1, Str); // Reads a whole line from the file.
          TextFileViewerForm.SynMemo1.Lines.Add(Str);
        until(EOF(File1)); // EOF(End Of File) keep reading new lines until end.
        CloseFile(File1);
      except
        //StatusMessage('File: changelog.txt IOERROR!', clYellow);
      end;
      {$I+}//Turn IO checking back on.
    end;
  //else
   //StatusMessage('File: changelog.txt does not exist!', clYellow);

   TextFileViewerForm.Caption:=Title;
   TextFileViewerForm.Show;
end;

initialization
  {$I textfileviewer.lrs}

end.

