unit rotstageunit;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, ExtCtrls, ComCtrls;

type

  { TFormRS }

  TFormRS = class(TForm)
    RSStatusBar: TStatusBar;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FormRS: TFormRS;

implementation

{ TFormRS }

procedure TFormRS.FormCreate(Sender: TObject);
begin

end;

initialization
  {$I rotstageunit.lrs}

end.

