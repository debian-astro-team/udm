unit arpmethod;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Grids;

type

  { TFormarpmethod }

  TFormarpmethod = class(TForm)
    ARPStatusMemo: TMemo;
    FixXPortButton: TButton;
    FixXPortGroupBox: TGroupBox;
    IPEdit: TEdit;
    FreeIPsLabel: TLabel;
    ChooseIPGroupBox: TGroupBox;
    InstructionsMemo: TMemo;
    InstructionsLabel: TLabel;
    StatusLabel: TLabel;
    IPsInUseGroupBox: TGroupBox;
    AssignedIPsLabel: TLabel;
    FindIPsButton: TButton;
    AssignedIPsStringGrid: TStringGrid;
    FreeIPsStringGrid: TStringGrid;
    RandomlyChooseIPButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure FindIPsButtonClick(Sender: TObject);
  private

  public

  end;

var
  Formarpmethod: TFormarpmethod;

implementation
uses
  Unit1,Process;

{ TFormarpmethod }

procedure TFormarpmethod.FindIPsButtonClick(Sender: TObject);
var
  s : ansistring;
begin
  //See if arp command is available
  //if RunCommand('/bin/bash',['-c','echo $PATH'],s) then
   ARPStatusMemo.Lines.Add('Running arp');
   Application.ProcessMessages;
   if RunCommand('arp',['-a'],s) then begin
     ARPStatusMemo.Lines.Add(s);
     //writeln(s);
   end else
     ARPStatusMemo.Lines.Add('The arp command cannot be accessed. Install it, or check the PATH.');

end;

procedure TFormarpmethod.FormShow(Sender: TObject);
begin
   ARPStatusMemo.Clear;
   ARPStatusMemo.Font.Name:=FixedFont;
end;

initialization
  {$I arpmethod.lrs}

end.

