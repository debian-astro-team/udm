object DLHeaderForm: TDLHeaderForm
  Left = 2180
  Height = 692
  Top = 199
  Width = 660
  Anchors = []
  Caption = 'Datalogging header'
  ClientHeight = 692
  ClientWidth = 660
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  Position = poScreenCenter
  LCLVersion = '2.2.4.0'
  object ScrollBox1: TScrollBox
    AnchorSideLeft.Control = Owner
    AnchorSideTop.Control = Owner
    AnchorSideRight.Control = Owner
    AnchorSideRight.Side = asrBottom
    AnchorSideBottom.Control = Owner
    AnchorSideBottom.Side = asrBottom
    Left = 0
    Height = 692
    Top = 0
    Width = 660
    HorzScrollBar.Page = 656
    VertScrollBar.Page = 672
    VertScrollBar.Tracking = True
    Anchors = [akTop, akLeft, akRight, akBottom]
    ClientHeight = 690
    ClientWidth = 658
    TabOrder = 0
    object SelectedGroupBox: TGroupBox
      AnchorSideLeft.Control = ScrollBox1
      AnchorSideTop.Control = ScrollBox1
      AnchorSideBottom.Control = ScrollBox1
      AnchorSideBottom.Side = asrBottom
      Left = 4
      Height = 690
      Top = 0
      Width = 652
      Anchors = [akTop, akLeft, akBottom]
      BorderSpacing.Left = 4
      Caption = 'Selected serial number:'
      ClientHeight = 667
      ClientWidth = 648
      TabOrder = 0
      object DataSupplierEntry: TLabeledEdit
        AnchorSideLeft.Control = InstrumentIDEntry
        AnchorSideTop.Control = InstrumentIDEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Hint = 'The institution and/or person who was responsible for setting up and acquiring the data. Since it is'#10'expected that skyglow data will be archived for generations, detailed contact information (e.g. email'#10'address) is unlikely to be as helpful as information about the institute that supplied the data. Users are'#10'free to provide contact information in the user comments section below.'
        Top = 89
        Width = 281
        EditLabel.Height = 21
        EditLabel.Width = 100
        EditLabel.Caption = 'Data supplier: '
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = DataSupplierEntryChange
      end
      object InstrumentIDEntry: TLabeledEdit
        AnchorSideLeft.Control = SerialNumber
        AnchorSideTop.Control = SerialNumber
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Hint = 'The instrument ID is a unique human readable name. Since the number of stations monitoring is'#10'currently small, it probably won''t be a problem for users to come up with a name on their own. In the'#10'future, when a skyglow measurement database is established, there should be a procedure to have'#10'names assigned. In the meantime, please register your name with christopher.kyba@wew.fu-berlin.de.'#10'Since the instrument ID is used in the filename, spaces and other characters outside of the set [A-Za-z0-'#10'9_-] are not permitted (use dash or underscore instead of space).'#10'Examples: SQM-RIVM1, Dahlem_tower_le'
        Top = 58
        Width = 281
        EditLabel.Height = 21
        EditLabel.Width = 105
        EditLabel.Caption = 'Instrument ID: '
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = InstrumentIDEntryChange
      end
      object LocationNameEntry: TLabeledEdit
        AnchorSideLeft.Control = DataSupplierEntry
        AnchorSideTop.Control = DataSupplierEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 120
        Width = 282
        EditLabel.Height = 21
        EditLabel.Width = 110
        EditLabel.Caption = 'Location name: '
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnChange = LocationNameEntryChange
      end
      object PositionEntry: TLabeledEdit
        AnchorSideLeft.Control = LocationNameEntry
        AnchorSideTop.Control = LocationNameEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 151
        Width = 282
        EditLabel.Height = 21
        EditLabel.Width = 180
        EditLabel.Caption = 'Position (lat, lon, elev(m)): '
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ReadOnly = True
        TabOrder = 2
        TabStop = False
      end
      object Label6: TLabel
        AnchorSideTop.Control = TZRegionBox
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = TZRegionBox
        Left = 102
        Height = 21
        Top = 186
        Width = 178
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        AutoSize = False
        BorderSpacing.Right = 3
        Caption = 'Local timezone region:'
        ParentColor = False
      end
      object TZRegionBox: TComboBox
        AnchorSideLeft.Control = PositionEntry
        AnchorSideTop.Control = PositionEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 29
        Top = 182
        Width = 282
        ItemHeight = 0
        Items.Strings = (
          'africa'
          'asia'
          'europe'
          'northamerica'
          'antarctica'
          'australasia'
          'etcetera'
          'southamerica'
        )
        OnChange = TZRegionBoxChange
        Style = csDropDownList
        TabOrder = 4
      end
      object TimeSynchEntry: TLabeledEdit
        AnchorSideLeft.Control = TZLocationBox
        AnchorSideTop.Control = TZLocationBox
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 244
        Width = 282
        EditLabel.Height = 21
        EditLabel.Width = 152
        EditLabel.Caption = 'Time Synchronization:'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        TabOrder = 5
        OnChange = TimeSynchEntryChange
      end
      object MovingStationaryPositionCombo: TComboBox
        AnchorSideLeft.Control = TimeSynchEntry
        AnchorSideTop.Control = TimeSynchEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Hint = 'GPS enabled overrides to Moving.'
        Top = 275
        Width = 282
        ItemHeight = 0
        Items.Strings = (
          'MOVING'
          'STATIONARY'
        )
        OnChange = MovingStationaryPositionComboChange
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object Label7: TLabel
        AnchorSideTop.Control = MovingStationaryPositionCombo
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = MovingStationaryPositionCombo
        Left = 83
        Height = 21
        Top = 280
        Width = 197
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        BorderSpacing.Right = 3
        Caption = 'Moving / Stationary position:'
        ParentColor = False
      end
      object MovingStationaryDirectionCombo: TComboBox
        AnchorSideLeft.Control = MovingStationaryPositionCombo
        AnchorSideTop.Control = MovingStationaryPositionCombo
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 306
        Width = 282
        ItemHeight = 0
        Items.Strings = (
          'MOVING'
          'FIXED'
        )
        OnChange = MovingStationaryDirectionComboChange
        TabOrder = 7
      end
      object Label8: TLabel
        AnchorSideTop.Control = MovingStationaryDirectionCombo
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = MovingStationaryDirectionCombo
        Left = 76
        Height = 21
        Top = 311
        Width = 204
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        BorderSpacing.Right = 3
        Caption = 'Moving / Fixed look direction:'
        ParentColor = False
      end
      object Label9: TLabel
        AnchorSideTop.Control = NumberOfChannelsEntry
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = NumberOfChannelsEntry
        Left = 136
        Height = 21
        Top = 343
        Width = 144
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        BorderSpacing.Right = 3
        Caption = 'Number of channels:'
        ParentColor = False
      end
      object NumberOfChannelsEntry: TSpinEdit
        AnchorSideLeft.Control = MovingStationaryPositionCombo
        AnchorSideTop.Control = MovingStationaryDirectionCombo
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 338
        Width = 225
        BorderSpacing.Top = 1
        MinValue = 1
        OnChange = NumberOfChannelsEntryChange
        TabOrder = 8
        Value = 1
      end
      object MeasurementDirectionPerChannelEntry: TLabeledEdit
        AnchorSideLeft.Control = SerialNumber
        AnchorSideTop.Control = FiltersPerChannelEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 432
        Width = 225
        EditLabel.Height = 21
        EditLabel.Width = 249
        EditLabel.Caption = 'Measurement direction per channel:'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        TabOrder = 9
        OnChange = MeasurementDirectionPerChannelEntryChange
      end
      object FieldOfViewEntry: TLabeledEdit
        AnchorSideLeft.Control = MeasurementDirectionPerChannelEntry
        AnchorSideTop.Control = MeasurementDirectionPerChannelEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 463
        Width = 226
        EditLabel.Height = 21
        EditLabel.Width = 157
        EditLabel.Caption = 'Field of view (degrees):'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        TabOrder = 10
        OnChange = FieldOfViewEntryChange
      end
      object TZLocationBox: TComboBox
        AnchorSideLeft.Control = TZRegionBox
        AnchorSideTop.Control = TZRegionBox
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 33
        Top = 211
        Width = 282
        ItemHeight = 0
        OnChange = TZLocationBoxChange
        Style = csDropDownList
        TabOrder = 11
      end
      object Label11: TLabel
        AnchorSideTop.Control = TZLocationBox
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = TZLocationBox
        Left = 129
        Height = 21
        Top = 217
        Width = 151
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        BorderSpacing.Right = 3
        Caption = 'Local timezone name:'
        ParentColor = False
      end
      object UserComment1: TLabeledEdit
        AnchorSideLeft.Control = FieldOfViewEntry
        AnchorSideTop.Control = FieldOfViewEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 494
        Width = 284
        EditLabel.Height = 21
        EditLabel.Width = 79
        EditLabel.Caption = 'Comments:'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        TabOrder = 12
        OnChange = UserComment1Change
      end
      object UserComment2: TEdit
        AnchorSideLeft.Control = UserComment1
        AnchorSideTop.Control = UserComment1
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 525
        Width = 284
        OnChange = UserComment2Change
        TabOrder = 13
      end
      object UserComment3: TEdit
        AnchorSideLeft.Control = UserComment2
        AnchorSideTop.Control = UserComment2
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 556
        Width = 284
        OnChange = UserComment3Change
        TabOrder = 14
      end
      object UserComment4: TEdit
        AnchorSideLeft.Control = UserComment3
        AnchorSideTop.Control = UserComment3
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 587
        Width = 284
        OnChange = UserComment4Change
        TabOrder = 15
      end
      object UserComment5: TEdit
        AnchorSideLeft.Control = UserComment4
        AnchorSideTop.Control = UserComment4
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 618
        Width = 284
        OnChange = UserComment5Change
        TabOrder = 16
      end
      object CoverOffsetEntry: TLabeledEdit
        AnchorSideLeft.Control = NumberOfChannelsEntry
        AnchorSideTop.Control = NumberOfChannelsEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Hint = 'this is the difference in reading caused by the weatherproof housing.'#10'For people using the standard housing from Unihedron the value is -0.11.'
        Top = 370
        Width = 122
        BorderSpacing.Top = 1
        EditLabel.Height = 21
        EditLabel.Width = 88
        EditLabel.Caption = 'Cover Offset:'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ParentShowHint = False
        ShowHint = True
        TabOrder = 18
        OnChange = CoverOffsetEntryChange
      end
      object SerialNumber: TLabeledEdit
        Left = 283
        Height = 31
        Top = 27
        Width = 131
        Anchors = []
        EditLabel.Height = 21
        EditLabel.Width = 103
        EditLabel.Caption = 'Serial Number:'
        EditLabel.ParentColor = False
        LabelPosition = lpLeft
        ReadOnly = True
        TabOrder = 17
        TabStop = False
      end
      object FiltersPerChannelEntry: TComboBox
        AnchorSideLeft.Control = CoverOffsetEntry
        AnchorSideTop.Control = CoverOffsetEntry
        AnchorSideTop.Side = asrBottom
        Left = 283
        Height = 31
        Top = 401
        Width = 225
        ItemHeight = 0
        ItemIndex = 0
        Items.Strings = (
          'HOYA CM-500'
        )
        OnChange = FiltersPerChannelEntryChange
        TabOrder = 19
        Text = 'HOYA CM-500'
      end
      object Label10: TLabel
        AnchorSideTop.Control = FiltersPerChannelEntry
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Control = FiltersPerChannelEntry
        Left = 148
        Height = 21
        Top = 406
        Width = 135
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Filters per channel: '
        ParentColor = False
      end
      object InvalidInstrumentID: TLabel
        AnchorSideLeft.Control = InstrumentIDEntry
        AnchorSideLeft.Side = asrBottom
        AnchorSideTop.Control = InstrumentIDEntry
        AnchorSideTop.Side = asrCenter
        Left = 569
        Height = 21
        Hint = 'Since the instrument ID is used in '#10'the filename, spaces and other '#10'characters outside of the set [A-Za-z0-9_-] '#10'are not permitted (use dash or '#10'underscore instead of space).'
        Top = 63
        Width = 46
        BorderSpacing.Left = 5
        Caption = 'Invalid'
        Font.Color = clRed
        ParentColor = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Visible = False
      end
      object EditPositionButton: TButton
        AnchorSideLeft.Control = PositionEntry
        AnchorSideLeft.Side = asrBottom
        AnchorSideTop.Control = PositionEntry
        AnchorSideTop.Side = asrCenter
        AnchorSideRight.Side = asrBottom
        Left = 568
        Height = 31
        Top = 151
        Width = 52
        BorderSpacing.Left = 3
        Caption = 'Edit'
        OnClick = EditPositionButtonClick
        TabOrder = 20
      end
      object CloseButton: TButton
        AnchorSideLeft.Side = asrBottom
        AnchorSideRight.Side = asrBottom
        AnchorSideBottom.Control = UserComment5
        AnchorSideBottom.Side = asrBottom
        Left = 569
        Height = 31
        Top = 618
        Width = 51
        Anchors = [akBottom]
        BorderSpacing.Left = 3
        Caption = 'Close'
        OnClick = CloseButtonClick
        TabOrder = 21
      end
      object PDFDocButton: TButton
        Left = 283
        Height = 31
        Hint = 'PDF definitions'
        Top = -7
        Width = 75
        Anchors = []
        Caption = 'PDF'
        OnClick = PDFDocButtonClick
        TabOrder = 22
      end
      object DefinitionsLink: TLabel
        AnchorSideLeft.Side = asrBottom
        Left = 368
        Height = 21
        Top = -6
        Width = 189
        Anchors = []
        BorderSpacing.Left = 4
        Caption = 'darksky.org/measurements'
        Font.Color = clBlue
        ParentColor = False
        ParentFont = False
        OnClick = DefinitionsLinkClick
        OnMouseEnter = DefinitionsLinkMouseEnter
        OnMouseLeave = DefinitionsLinkMouseLeave
      end
    end
  end
end
