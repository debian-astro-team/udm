unit viewlog;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil
  //, SynMemo
  , SynEdit, LResources, Forms, Controls,
  Graphics, Dialogs, StdCtrls;

type

  { TForm5 }

  TForm5 = class(TForm)
    SaveLog: TButton;
    SynEdit1: TSynEdit;
    procedure SaveLogClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form5: TForm5;

implementation

uses
  dateutils
  , Unit1
  , appsettings //For saving and restoring hotkeys.
  ;

{ TForm5 }

{ Save log to a file. }
procedure TForm5.SaveLogClick(Sender: TObject);
var
  OutFileName, LogString:String;
  LogFilename: TextFile;
begin
     //Save contents to standard logging area
    OutFileName:=RemoveMultiSlash(
                                  appsettings.LogsDirectory+
                                  DirectorySeparator+
                                  Format('log_%s.txt',
                                  [FormatDateTime('yyyy-mm-dd_hhnnss', Now)]));
    AssignFile(LogFilename,OutFileName);

      try
        Rewrite(LogFilename); //create the file

        for LogString in SynEdit1.Lines do begin
          Writeln(LogFilename, LogString);
        end;
      except
          on E: Exception do
             ShowMessage(
                         OutFileName+sLineBreak
                         + 'Error: '+E.ClassName+sLineBreak
                         + E.Message);
          //MessageDlg('ERROR! IORESULT','ERROR! IORESULT: '
          //                   + IntToStr(IOResult)
          //                   + ' during LogCalButtonClick',mtWarning,[mbOK],0);
      end;
      CloseFile(LogFilename);

     //Message about where log was saved to
     MessageDlg('Log file stored in:' + sLineBreak  + OutFileName, mtInformation,[mbOK],0);

end;

initialization
  {$I viewlog.lrs}

end.

