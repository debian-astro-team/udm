unit VectorProduct;

{$mode objfpc}

interface

type
  Tvector = record
    x, y, z: double
  end;
 function dotProduct(a, b: Tvector): double;
 function crossProduct(a, b: Tvector): Tvector;
 function scalarTripleProduct(a, b, c: Tvector): double;
 function vectorTripleProduct(a, b, c: Tvector): Tvector;

implementation

uses
Classes, SysUtils;

function dotProduct(a, b: Tvector): double;
begin
  dotProduct := a.x*b.x + a.y*b.y + a.z*b.z;
end;

function crossProduct(a, b: Tvector): Tvector;
begin
  crossProduct.x := a.y*b.z - a.z*b.y;
  crossProduct.y := a.z*b.x - a.x*b.z;
  crossProduct.z := a.x*b.y - a.y*b.x;
end;

function scalarTripleProduct(a, b, c: Tvector): double;
begin
  scalarTripleProduct := dotProduct(a, crossProduct(b, c));
end;

function vectorTripleProduct(a, b, c: Tvector): Tvector;
begin
  vectorTripleProduct := crossProduct(a, crossProduct(b, c));
end;
end.

