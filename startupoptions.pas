unit startupoptions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, SynMemo, appsettings
  ;

type

  { TStartUpOptionsForm }

  TStartUpOptionsForm = class(TForm)
    UDMArgumentsLabeledEdit: TLabeledEdit;
    StartUpSettingsEdit: TLabeledEdit;
    StartupInstructions: TMemo;
    SynMemo1: TSynMemo;
    procedure FormActivate(Sender: TObject);
    procedure StartUpSettingsEditChange(Sender: TObject);
  private

  public

  end;

var
  StartUpOptionsForm: TStartUpOptionsForm;
procedure fillview();

implementation

uses
  Unit1
  , header_utils
  ;

{ TStartUpOptionsForm }

procedure fillview();
var
 File1: TextFile;
 Str: String;
 Filename:String;
begin
  Filename:='commandlineoptions.txt';
  {Display Filename }
  if FileExists(appsettings.DataDirectory+Filename) then
    begin
      AssignFile(File1,appsettings.DataDirectory+Filename);
      {$I-}//Temporarily turn off IO checking
      try
        Reset(File1);
        StartUpOptionsForm.SynMemo1.Clear;
        repeat
          Readln(File1, Str); // Reads a whole line from the file.
          StartUpOptionsForm.SynMemo1.Lines.Add(Str);
        until(EOF(File1)); // EOF(End Of File) keep reading new lines until end.
        CloseFile(File1);
      except
        //StatusMessage('File: changelog.txt IOERROR!', clYellow);
      end;
      {$I+}//Turn IO checking back on.
    end;
  //else
   //StatusMessage('File: changelog.txt does not exist!', clYellow);
   StartUpOptionsForm.Show;

end;

procedure TStartUpOptionsForm.StartUpSettingsEditChange(Sender: TObject);
begin
  StartUpSettings:=StartUpSettingsEdit.Text;
  vConfigurations.WriteString('StartUp','Settings',StartUpSettings);
end;

procedure TStartUpOptionsForm.FormActivate(Sender: TObject);
var
 i:integer;
 s:string='';
begin

  for i := 1 to paramCount() do
      begin
          s:= s + ' ' + paramStr(i);
      end;
  UDMArgumentsLabeledEdit.Text:=s;

  StartUpSettingsEdit.Text:=StartUpSettings;

end;

initialization
  {$I startupoptions.lrs}

end.

