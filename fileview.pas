unit fileview;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, SynEdit, TAGraph, LResources,
  Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls;

type

  { TForm2 }

  TForm2 = class(TForm)
    Chart1: TChart;
    Memo1: TMemo;
    PageControl1: TPageControl;
    TextTab: TTabSheet;
    GraphTab: TTabSheet;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form2: TForm2; 

implementation

initialization
  {$I fileview.lrs}

end.

