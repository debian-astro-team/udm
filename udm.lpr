program udm;

{$mode objfpc}{$H+}
//{$DEFINE debug}     // do this here or you can define a -dDEBUG in Project Options/Other/Custom Options, i.e. in a build mode so you can set up a Debug with leakview and a Default build mode without it

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, tachartlazaruspkg, printer4lazarus, fileview,
  About, dlheader, appsettings, viewlog, splash, logcont, header_utils,
  unitdirectorylist, convertlogfileunit, convertoldlog, convertoldlogfile,
  vector_utils, vector, VectorProduct, dlerase, dlclock, dlretrieve,
  textfileviewer, comterm, worldmap, plotter, dattimecorrect, datlocalcorrect,
  correct49to56, dattokml, avgtool, date2dec, concattool, CloudRemUnit, 
startupoptions, configbrowser, arpmethod;

//{$IFDEF WINDOWS}{$R udm.rc}{$ENDIF}

{$R *.res}

begin
  Application.Scaled:=True;
   {$IFDEF DEBUG}
  // Assuming your build mode sets -dDEBUG in Project Options/Other when defining -gh
  // This avoids interference when running a production/default build without -gh

  // Set up -gh output for the Leakview package:
  if FileExists('heap.trc') then
    DeleteFile('heap.trc');
  SetHeapTraceOutput('heap.trc');
  {$ENDIF DEBUG}

  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TDLHeaderForm, DLHeaderForm);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TfrmSplash, frmSplash);
  Application.CreateForm(TFormLogCont, FormLogCont);
  Application.CreateForm(TDirectories, Directories);
  Application.CreateForm(Tconvertdialog, convertdialog);
  Application.CreateForm(TConvertOldLogForm, ConvertOldLogForm);
  Application.CreateForm(TVectorForm, VectorForm);
  Application.CreateForm(TFormDLErase, FormDLErase);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TDLRetrieveForm, DLRetrieveForm);
  Application.CreateForm(TTextFileViewerForm, TextFileViewerForm);
  Application.CreateForm(TComTermForm, ComTermForm);
  Application.CreateForm(TFormWorldmap, FormWorldmap);
  Application.CreateForm(TPlotterForm, PlotterForm);
  Application.CreateForm(Tdattimecorrectform, dattimecorrectform);
  Application.CreateForm(Tdatlocalcorrectform, datlocalcorrectform);
  Application.CreateForm(TCorrectForm, CorrectForm);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TStartUpOptionsForm, StartUpOptionsForm);
  Application.CreateForm(TForm10, Form10);
  Application.CreateForm(TConcatToolForm, ConcatToolForm);
  Application.CreateForm(TCloudRemMilkyWay, CloudRemMilkyWay);
  Application.CreateForm(TConfigBrowserForm, ConfigBrowserForm);
  Application.CreateForm(TFormarpmethod, Formarpmethod);
  Application.Run;
end.

