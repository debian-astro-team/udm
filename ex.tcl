set timeout 20
set sftparameters [lindex $argv 0]
set password [lindex $argv 1]
set localdir [lindex $argv 2]
set remotedir [lindex $argv 3]
set imagename [lindex $argv 4]
spawn sftp "$sftparameters"
expect "assword:"
send "$password\r"
expect "sftp>"
send "cd ${remotedir}\r"
expect "sftp>"
send "put ${localdir}sqm.html\r"
expect "sftp>"
send "put ${imagename}\r"
expect "sftp>"
send "quit\r"
exit
