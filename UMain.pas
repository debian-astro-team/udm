unit UMain;

{$mode objfpc}{$H+}

interface

uses

type

  { TFormContourmap }

  TFormContourmap = class(TForm)
    procedure Button1Click(Sender: TObject);
    procedure Chart1ColorMapSeries1Calculate(const AX,AY: Double; out AZ: Double);
    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure CheckBox1Change(Sender: TObject);
    procedure ShowDotsCheckBoxChange(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ShowlinesButtonClick(Sender: TObject);
    procedure ShowLinesCheckBoxChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 


implementation

uses Math;

{ TFormContourmap }
{******************************************************************************}

Initialization
  {$I UMain.lrs}


END.
